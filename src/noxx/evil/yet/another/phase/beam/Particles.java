package noxx.evil.yet.another.phase.beam;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import noxx.evil.yet.another.phase.beam.LiveWallpaper.myMetrics;

public class Particles {
	
	public Sprite[] particles;
	private float tempX, tempY, tempDx, tempDy, tempScaleX, tempScaleY;
	private Random gen;
	private int N_PARTICLES;
	private boolean textureLoaded = false;
	public int[] mTextureName;	
	public int[] mCrop;
	public int imageHeight, imageWidth;
	public int visibleParticles = 15;
	public float speedMultiplier = 1.0f;

	public Particles(int _N_PARTICLES, myMetrics display) {
    	gen = new Random();
		N_PARTICLES = _N_PARTICLES;
    	particles = new Sprite[N_PARTICLES];
    	for (int i = 0; i < N_PARTICLES; i++) {
    		tempX = gen.nextFloat() * (float)display.width; 
    		tempY = gen.nextFloat() * (float)display.height;
    		tempScaleX = tempScaleY = gen.nextFloat()*2.0f + 0.75f;//0.75f;
//    		tempDx = (gen.nextFloat() * 30f + 10) * tempScaleX;
//    		tempDy = (gen.nextFloat() * 15f + 5) * tempScaleY;
    		tempDx = 20 * (float)(Math.pow(tempScaleX, 1.5));
    		tempDy = 10 * (float)(Math.pow(tempScaleY, 1.5));
    		particles[i] = new Sprite(tempX, tempY, tempDx, tempDy, tempScaleX, tempScaleY);
    	}
	}
	
	public void loadTexture(GL10 gl, Resources resources, int resourceId) {
		if (!textureLoaded) {
    		mTextureName = new int[1];
	    	// Generate Texture ID
	    	gl.glGenTextures(1, mTextureName, 0);
	    	// Did generate work? 
	    	assert gl.glGetError() == GL10.GL_NO_ERROR;
	    	
	    	InputStream is = resources.openRawResource(resourceId);
	    	Bitmap bitmap;
		    try {
		    	bitmap = BitmapFactory.decodeStream(is);
		    } finally {
		    	try {
		    		is.close();
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    }
		
		    // Bind texture id texturing target (we want 2D of course)
		    gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
		    	
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
		    	     	 
		    // Build our crop texture to be the size of the bitmap (ie full texture)
		    mCrop = new int[4];
		    mCrop[0] = 0;
		    mCrop[1] = imageHeight = bitmap.getHeight();
		    mCrop[2] = imageWidth = bitmap.getWidth();
		    mCrop[3] = -bitmap.getHeight();
		    	
		    ((GL11)gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mCrop, 0);
		    	 
		    // Magic Android function that setups up the internal formatting
		    // of the bitmap for OpenGL ES
		    GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		    	 
		    // Did texImage work?
		    assert gl.glGetError() == GL10.GL_NO_ERROR;
				
		    bitmap.recycle();
	    	
	    	for (int i = 0; i < N_PARTICLES; i++) {
	    		particles[i].imageHeight = imageHeight;
	    		particles[i].imageWidth = imageWidth;
	    	}
	    	
	    	textureLoaded = true;
    	}
	}
	
	public void reloadTexture(GL10 gl, Resources resources, int resourceId) {
		gl.glDeleteTextures(1, mTextureName, 0);
		textureLoaded = false;
		loadTexture(gl, resources, resourceId);
	}
	
	public void drawSprites(GL10 gl) {
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
		for (int i = 0; i < visibleParticles; i++) {
	    	((GL11Ext)gl).glDrawTexfOES(particles[i].x, particles[i].y, 0, imageWidth * particles[i].scaleX, imageHeight * particles[i].scaleY);
		}
	}
	
	public void updateSprites(float delta, myMetrics display) {
		for (int i = 0; i < visibleParticles; i++) {
			particles[i].update(delta, display, speedMultiplier);
		}
	}
}