package noxx.evil.yet.another.phase.beam;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import net.rbgrn.android.glwallpaperservice.GLWallpaperService;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class LiveWallpaper extends GLWallpaperService
{

	static final String SHARED_PREFS_NAME = "YAPB Preferences";
	private SharedPreferences mPreferences;
	
	public LiveWallpaper() {
		super();
	}
	
	public Engine onCreateEngine() {
		MyEngine engine = new MyEngine(this.getResources());
		return engine;
	}
	
	class MyEngine extends GLEngine implements SharedPreferences.OnSharedPreferenceChangeListener {
		
		ClearRenderer renderer;
		private int width, height;
		private int[] ballResources = new int[]{R.drawable.ball_candy, 
												R.drawable.ball_halloween, 
												R.drawable.ball_ocean,
												R.drawable.ball_oil,
												R.drawable.ball_earth,
												R.drawable.ball_sky,
												R.drawable.ball_neon,
												R.drawable.ball_sun};
		
		private int[] backgroundResources = new int[] {	R.drawable.borealis_candy, 
														R.drawable.borealis_halloween,
														R.drawable.borealis_ocean,
														R.drawable.borealis_oil,
														R.drawable.borealis_earth,
														R.drawable.borealis_sky,
														R.drawable.borealis_neon,
														R.drawable.borealis_sun};
													
		public MyEngine(Resources resources) {
			super();
			DisplayMetrics metrics = new DisplayMetrics();
			metrics = resources.getDisplayMetrics();
			width = metrics.widthPixels;
			height = metrics.heightPixels;
//			try {
//				this.setEGLConfigChooser(false);
//				renderer = new ClearRenderer(resources, width, height, this);
//				setRenderer(renderer);
//				setRenderMode(RENDERMODE_CONTINUOUSLY);
//			} catch (IllegalArgumentException e) {
				this.setEGLConfigChooser(8,8,8,8,0,0);
				renderer = new ClearRenderer(resources, width, height);
				setRenderer(renderer);
				setRenderMode(RENDERMODE_CONTINUOUSLY);
//			}
			
			mPreferences = LiveWallpaper.this.getSharedPreferences(SHARED_PREFS_NAME, 0);
			mPreferences.registerOnSharedPreferenceChangeListener(this);
			onSharedPreferenceChanged(mPreferences, null);
			onSharedPreferenceChanged(mPreferences, "color_scheme_key");
		}

		public void onDestroy() {
			super.onDestroy();
			if (renderer != null) {
				//renderer.release(); // assuming yours has this method - it should!
			}
			renderer = null;
		}

		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {			
			try {
				int newFPS = sharedPreferences.getInt("fps", 30);
				renderer.thousandOverFPS = 1000 / newFPS;
				int newParticleCount = sharedPreferences.getInt("particleCount", 15);
				renderer.particles.visibleParticles = newParticleCount;
				int newSpeedMult = sharedPreferences.getInt("speedMultiplier", 100);
				renderer.particles.speedMultiplier = (float)newSpeedMult / 100.0f;
				if (key.equals("color_scheme_key")) {
					int newTextureIndex = Integer.parseInt(sharedPreferences.getString("color_scheme_key", "0"));
					renderer.newBallResourceId = ballResources[newTextureIndex];
					renderer.newBackgroundResourceId = backgroundResources[newTextureIndex];
					renderer.needsReloading = true;
				}
			} catch (NullPointerException e) {
				// Move along, nothing to see here...
			}
			
		}
	}
	
	class ClearRenderer implements GLWallpaperService.Renderer {
		private Resources _resources;
		private Particles particles;
		private Sprite background;
		private long lastTime;
		private long currentTime;
		private float delta;
		private myMetrics display;
		private final int N_PARTICLES = 15;
		public boolean needsReloading = false;
		public int newBallResourceId = R.drawable.ball_candy;
		public int newBackgroundResourceId = R.drawable.borealis_candy;
		
		public int thousandOverFPS = 33;
		private long dt;
//		private int fps = 0;
//		private float tempDt = 0;
		
		private final float LUM = 0.9f;
		
	    public ClearRenderer(Resources resources, int width, int height) {
	    	_resources = resources;
	    	display = new myMetrics(width, height);
	    	particles = new Particles(N_PARTICLES, display);
	    	background = new Sprite(0,0,0,0, 0, 0);
	    	needsReloading = true;
	    }
	    
	    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	    
			//engine.getSurfaceHolder().setFormat(PixelFormat.RGBA_8888);	
	    	
	    	gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST); 
	    	
	    	gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	    	gl.glShadeModel(GL10.GL_FLAT);
	    	gl.glDisable(GL10.GL_DITHER);
	    	gl.glDepthMask(false);
	    	gl.glEnable(GL10.GL_TEXTURE_2D); 
	    	
	    	gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_DST_ALPHA);	    	
	    	
	    	gl.glDisable(GL10.GL_LIGHTING);
	    	gl.glDisable(GL10.GL_DEPTH_TEST);
	    	
	    	gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE); 
	    	gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	    	
	    	gl.glColor4f(LUM, LUM, LUM, 1.0f);
	    	gl.glEnable(GL10.GL_BLEND);
	    	
	    	if (needsReloading) {
	    		particles.loadTexture(gl, _resources, newBallResourceId);
	    		background.loadTexture(gl, _resources, newBackgroundResourceId);
	    	}

	    	background.scaleX = (float)display.width / (float)background.imageWidth;
	    	background.scaleY = (float)display.height / (float)background.imageHeight;
	    	
	    	lastTime = System.currentTimeMillis();
	    	
	    }

	    public void onSurfaceChanged(GL10 gl, int w, int h) {
	    	gl.glViewport(0, 0, w, h);
	    	display.width = w;
	    	display.height = h;
	    		    	
	    	background.scaleX = (float)w / (float)background.imageWidth;
	    	background.scaleY = (float)h / (float)background.imageHeight;
	    	
	    	float ratio = (float) w / h;
	    	gl.glMatrixMode(GL10.GL_PROJECTION);
	    	gl.glLoadIdentity();
	    	gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
	    	
	    	
	    	gl.glOrthof(0.0f, display.width, 0.0f, display.height, 0.0f, 1.0f);
	    	gl.glMatrixMode(GL10.GL_MODELVIEW);
	    	
	    }

	    public void onDrawFrame(GL10 gl) {
	    	gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	    	
	    	if (needsReloading) {
	    		particles.reloadTexture(gl, _resources, newBallResourceId);
	    		background.reloadTexture(gl, _resources, newBackgroundResourceId);
	    		needsReloading = false;
	    	}
	    	
	    	currentTime = System.currentTimeMillis();
	    	delta = (float)(currentTime - lastTime) / 1000.0f;

	    	
	    	dt = currentTime - lastTime;
	    	if (dt < thousandOverFPS) {
				try {
					Thread.sleep(thousandOverFPS - dt);
//					tempDt += thousandOverFPS - dt;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
	    	}

	    	lastTime = currentTime;
	    	
//	    	tempDt += dt;
//	    	fps += 1;
//	    	if (tempDt >= 1000) {
//	    		Log.d("FPS", String.format("%d", fps));
//	    		tempDt = 0;
//	    		fps = 0;
//	    	}


	    	background.draw(gl);

	    	particles.updateSprites(delta, display);
	    	particles.drawSprites(gl);

	    }
	}
	
	public class myMetrics {
		public int width, height;
		public myMetrics(int w, int h) {
			width = w;
			height = h;
		}
	}
}